﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Transform tf;
    public float speed = 1.0f;
    private Rigidbody2D rb;
    
    // Start is called before the first frame update
    void Start()
    {
        tf = GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow)) //move forward
        {
            tf.position -= tf.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow)) //move forward
        {
            tf.position += tf.right * speed * Time.deltaTime;
        }
        if (rb.velocity.x > 0.1f)
        {
            GetComponent<Animator>().Play("PlayerWalk");
            GetComponent<SpriteRenderer>().flipX = false;
        }
        else if (rb.velocity.x < -0.1f)
        {
            GetComponent<Animator>().Play("PlayerWalk");
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<Animator>().Play("PlayerIdle");
        }
    }
}
